A simple NodeJS service that Reads & Writes files from & to host a directory

> `GET /configurations`  
`POST /configurations`  
`GET /readiness`  

Used in Part 3 of an introductory blog series `Building a Web Service with Minikube` (in-progress)

#### To run locally

`npm start`

Navigate to `localhost:3000`

#### To run w/ Docker

Build Image & run container  
`docker build -t basic-host-mount .`  
`docker run -p 3000:3000 -d -v $(pwd)/configFiles:/opt/node_app/configFiles basic-host-mount`

#### To run w/ Kubernetes

`cd` into the `_kubeManifests` directory & apply the current context

`$ kk apply -f .`

#### Resources
* https://docs.docker.com/storage/bind-mounts/
* https://docs.docker.com/storage/volumes/
* https://kubernetes.io/docs/setup/learning-environment/minikube/#mounted-host-folders
* https://minikube.sigs.k8s.io/docs/handbook/mount/
* https://kubernetes.io/docs/concepts/storage/volumes/#hostpath
* https://github.com/BretFisher/node-docker-good-defaults/blob/master/Dockerfile
* https://github.com/BretFisher/node-docker-good-defaults/blob/69c923bc646bc96003e9ada55d1ec5ca943a1b19/bin/www
* https://github.com/RisingStack/kubernetes-graceful-shutdown-example/blob/master/src/index.js

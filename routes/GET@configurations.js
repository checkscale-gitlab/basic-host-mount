const path = require('path');
const fs = require('fs');
const fsPromises = fs.promises;


const configDir = path.resolve('configFiles');

async function readConfigs() {
  const fileNames = await fsPromises.readdir(`${configDir}`, 'utf8');
  console.log({ fileNames });
  const promises = fileNames.map(fileName => readFile(fileName));

  return await Promise.all(promises);
}

async function readFile(fileName) {
  const fileContents =  await fsPromises.readFile(`${configDir}/${fileName}`, 'utf8');
  return JSON.parse(fileContents);
}

async function getConfigurations(req, res) {
  const configs = await readConfigs();

  res.writeHead(200, {
    'Content-Type': 'application/json',
    'Content-Length': Buffer.byteLength(JSON.stringify({ configs }))
  });

  res.write(JSON.stringify({ configs }));
  res.end();
}


module.exports = getConfigurations;
